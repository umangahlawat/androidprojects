package com.example.android.dauralacity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class PondsFragment extends Fragment {

    public PondsFragment(){

    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_list, container, false);
        final ArrayList<Place> places = new ArrayList<>();
        places.add(new Place(getString(R.string.pond_daurala_baccho)));
        places.add(new Place(getString(R.string.pond_daurala_khora)));
        places.add(new Place(getString(R.string.pond_daurala_namoharpuri)));
        places.add(new Place(getString(R.string.pond_daurala_peer)));
        places.add(new Place(getString(R.string.pond_daurala_shegarhi)));
        places.add(new Place(getString(R.string.pond_daurala_south)));
        places.add(new Place(getString(R.string.pond_daurala_tuklaqabad)));

        PlaceAdapter wordItemsAdapter = new PlaceAdapter(getActivity(), places, R.color.category_phrases);
        ListView listView = (ListView)rootView.findViewById(R.id.list);
        listView.setAdapter(wordItemsAdapter);

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
