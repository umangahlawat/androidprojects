package com.example.android.dauralacity;

public class Place {
    private String name;
    private int imageResourceId = -1;

    public static final int NO_IMAGE_STATE = -1;

    public Place(String name){
        this.name = name;
    }

    public Place(String name, int imageResourceId){
        this.name = name;
        this.imageResourceId = imageResourceId;
    }

    public String getName() {
        return name;
    }
    public int getImageResourceId() {
        return imageResourceId;
    }
    public  boolean getImageState() {
        return imageResourceId != NO_IMAGE_STATE ? true : false;
    }
}
