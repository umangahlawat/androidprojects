package com.example.android.dauralacity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class IndustriesFragment extends Fragment {
    public IndustriesFragment(){

    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_list, container,false);
        final ArrayList<Place> places = new ArrayList<>();
        places.add(new Place(getString(R.string.industry_daurala_agri)));
        places.add(new Place(getString(R.string.industry_daurala_Chemical)));
        places.add(new Place(getString(R.string.industry_daurala_food)));
        places.add(new Place(getString(R.string.industry_daurala_orgnic)));
        places.add(new Place(getString(R.string.industry_daurala_Steel)));
        places.add(new Place(getString(R.string.industry_daurala_sugar)));

        PlaceAdapter wordItemsAdapter = new PlaceAdapter(getActivity(), places, R.color.category_family);
        ListView listView = (ListView)rootView.findViewById(R.id.list);
        listView.setAdapter(wordItemsAdapter);

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
