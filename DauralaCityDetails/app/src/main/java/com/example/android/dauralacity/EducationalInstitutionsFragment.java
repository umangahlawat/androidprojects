package com.example.android.dauralacity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class EducationalInstitutionsFragment extends Fragment {

    public EducationalInstitutionsFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_list, container, false);
        final ArrayList<Place> places = new ArrayList<>();
        places.add(new Place( getString(R.string.edu_daurala_cms)));
        places.add(new Place(getString(R.string.edu_daurala_hotel)));
        places.add(new Place( getString(R.string.edu_daurala_kisaan)));
        places.add(new Place(getString(R.string.edu_daurala_poly)));
        places.add(new Place( getString(R.string.edu_daurala_primary)));
        places.add(new Place( getString(R.string.edu_daurala_saraswati)));
        places.add(new Place( getString(R.string.edu_daurala_shriram)));

        PlaceAdapter wordItemsAdapter = new PlaceAdapter(getActivity(), places, R.color.category_numbers);
        ListView listView = (ListView)(rootView.findViewById(R.id.list));
        listView.setAdapter(wordItemsAdapter);

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
