package com.example.android.dauralacity;

import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

public class ReligiousPlacesFragment extends Fragment {
    public ReligiousPlacesFragment(){

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.place_list, container, false);

        ArrayList<Place> places = new ArrayList<>();
        places.add(new Place(getString(R.string.religious_daurala_shivji), R.drawable.shivji));
        places.add(new Place(getString(R.string.religious_daurala_jain), R.drawable.jain));
        places.add(new Place(getString(R.string.religious_daurala_shivmandir), R.drawable.nagarpanchayat));
        places.add(new Place(getString(R.string.religious_daurala_kuti), R.drawable.kuti));
        places.add(new Place(getString(R.string.religious_daurala_chruch), R.drawable.chruch));
        places.add(new Place(getString(R.string.religious_daurala_masjid), R.drawable.masjid));
        places.add(new Place(getString(R.string.religious_daurala_gurudwara), R.drawable.gurudwara));

        PlaceAdapter wordItemsAdapter = new PlaceAdapter(getActivity(), places, R.color.category_colors);
        ListView listView = (ListView)rootView.findViewById(R.id.list);
        listView.setAdapter(wordItemsAdapter);

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}

