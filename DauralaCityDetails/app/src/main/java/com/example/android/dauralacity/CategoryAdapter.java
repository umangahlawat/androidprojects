package com.example.android.dauralacity;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class CategoryAdapter extends FragmentPagerAdapter {
    private Context mContext = null;

    public CategoryAdapter(Context context, FragmentManager fm){
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mContext = context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 : return new EducationalInstitutionsFragment();
            case 1 : return new ReligiousPlacesFragment();
            case 2 : return new IndustriesFragment();
            case 3 : return new PondsFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position){
            case 0 : return mContext.getString(R.string.category_educational_institutes);
            case 1 : return mContext.getString(R.string.category_religious_places);
            case 2 : return mContext.getString(R.string.category_industries);
            case 3 : return mContext.getString(R.string.category_ponds);
        }
        return "";
    }
}
