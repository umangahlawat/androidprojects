package com.example.android.dauralacity;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class PlaceAdapter extends ArrayAdapter<Place> {
    int bgColorResourceId;
    public PlaceAdapter(Activity context, ArrayList<Place> placeArrayList, int bgColorResourceId){
        super(context, 0, placeArrayList);
        this.bgColorResourceId = bgColorResourceId;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }
        TextView defaultText = listItemView.findViewById(R.id.engText);
        ImageView imageView = listItemView.findViewById(R.id.image);
        LinearLayout textContainer = listItemView.findViewById(R.id.textContainer);

        Place currPlace = getItem(position);

        defaultText.setText(currPlace.getName());

        if(currPlace.getImageState()) {
            imageView.setImageResource(currPlace.getImageResourceId());
            imageView.setVisibility(View.VISIBLE);
        }else{
            imageView.setVisibility(View.GONE);
        }

        textContainer.setBackgroundColor(getContext().getColor(bgColorResourceId));
        return listItemView;
    }
}
