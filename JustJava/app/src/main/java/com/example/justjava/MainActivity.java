/**
 * IMPORTANT: Make sure you are using the correct package name.
 * This example uses the package name:
 * package com.example.android.justjava
 * If you get an error when copying this code into Android studio, update it to match teh package name found
 * in the project's AndroidManifest.xml file.
 **/

package com.example.justjava;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {
    private TextView quantityTextView;
    private TextView orderSummaryTextView;
    private Button incrementButton;
    private Button decrementButton;
    private Button orderButton;
    private CheckBox cbToppingWHipped;
    private CheckBox cbToppingChoco;
    private EditText etName;
    private int quantity = 1;
    public final static int PRICE_OF_CUP = 5;
    public final static int PRICE_OF_WHIPPED_CREAM = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        incrementButton = findViewById(R.id.increment_button);
        decrementButton = findViewById(R.id.decrement_button);
        orderButton = findViewById(R.id.order_button);
        quantityTextView = findViewById(R.id.quantity_text_view);
        orderSummaryTextView = findViewById(R.id.order_summary_text_view);
        cbToppingWHipped = findViewById(R.id.id_topping_whipped);
        cbToppingChoco = findViewById(R.id.id_topping_choco);
        etName = findViewById(R.id.edit_name);
        displayPrice("Amount due : " + calculatePrice(quantity));
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        if(etName.getText().toString().trim().length() == 0){
            Toast.makeText(this, "Please enter name to genrate bill !!", Toast.LENGTH_LONG).show();
            return;
        }
        displayQuantity(quantity);
        String msg = createOrderSummary(quantity);
        composeEmail(msg);
        //displayPrice(msg);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void increment(View view) {
        if(quantity < 100) {
            quantity++;
            if(quantity > 0){
                decrementButton.setClickable(true);
            }
        }
        if(quantity == 100){
            incrementButton.setClickable(false);
        }
        displayQuantity(quantity);
        displayPrice("Amount due : " + calculatePrice(quantity));
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void decrement(View view) {
        if(quantity > 1) {
            quantity--;
            if(quantity < 100){
                incrementButton.setClickable(true);
            }
        }
        if(quantity == 1){
            decrementButton.setClickable(false);
        }
        displayQuantity(quantity);
        displayPrice("Amount due : " + calculatePrice(quantity));
    }

    /**
     *  This method will update price of whipped cream topping on the screen based on quantity.
     */
    public void whippedCream(View view){
        displayPrice("Amount due : " + calculatePrice(quantity));
    }

    /**
     *  This method will update price of chocolate topping on the screen based on quantity.
     */
    public void chocolate(View view){
        displayPrice("Amount due : " + calculatePrice(quantity));
    }

    /**
     * This method will update price of coffee on the screen based on quantity.
     */
    public void displayPrice(String msg) {
        orderSummaryTextView.setText(msg);
    }

    /**
     * This method will return order summary based on quqntity of coffee.
     */
    public String createOrderSummary(int quantity) {
        return "Name : " + etName.getText() + "\nQuantity : " + quantity + "\nWhipped Cream added? : " + cbToppingWHipped.isChecked()
                + "\nChocolate added?: " + cbToppingChoco.isChecked() + "\nTotal : " + calculatePrice(quantity) +"\nThank You !!";
    }

    //Calulates the price[5$ per coffee] as per the quanitity passed as parameter
    private String calculatePrice(int quantity){
        double price = quantity*PRICE_OF_CUP;
        if(cbToppingWHipped.isChecked() && quantity > 0){
            price = price + (2*quantity);
        }
        if(cbToppingChoco.isChecked() && quantity > 0){
            price = price + (1.5*quantity);
        }
        return NumberFormat.getCurrencyInstance(Locale.US).format(price);
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void displayQuantity(int number) {
        quantityTextView.setText("" + number);
    }

    /*
    * Sending email to coffee shop n hitting order button
    */
    public void composeEmail(String msg) {
        String[] address = new String[]{"umangahlawat@gmail.com"};
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, address);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Coffee ordered by " + etName.getText().toString() );
        intent.putExtra(Intent.EXTRA_TEXT, msg);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
