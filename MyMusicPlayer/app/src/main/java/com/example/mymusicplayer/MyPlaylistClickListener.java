package com.example.mymusicplayer;

import android.content.Intent;
import android.view.View;

public class MyPlaylistClickListener implements View.OnClickListener {
    @Override
    public void onClick(View view) {
        Intent intent = new Intent(view.getContext(), MyPlaylistActivity.class);
        view.getContext().startActivity(intent);
    }
}
