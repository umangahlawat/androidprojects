package com.example.mymusicplayer;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MyPlaylistActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.song_list);
        ArrayList<Song> songs = new ArrayList<>();
        songs.add(new Song(this.getString(R.string.myplaylist_song_1), this.getString(R.string.myplaylist_singer_1)));
        songs.add(new Song(this.getString(R.string.myplaylist_song_2), this.getString(R.string.myplaylist_singer_1)));
        songs.add(new Song(this.getString(R.string.myplaylist_song_3), this.getString(R.string.myplaylist_singer_1)));
        songs.add(new Song(this.getString(R.string.myplaylist_song_4), this.getString(R.string.myplaylist_singer_1)));
        songs.add(new Song(this.getString(R.string.myplaylist_song_5), this.getString(R.string.myplaylist_singer_1)));
        songs.add(new Song(this.getString(R.string.myplaylist_song_6), this.getString(R.string.myplaylist_singer_2)));
        songs.add(new Song(this.getString(R.string.myplaylist_song_7), this.getString(R.string.myplaylist_singer_2)));
        songs.add(new Song(this.getString(R.string.myplaylist_song_8), this.getString(R.string.myplaylist_singer_2)));
        songs.add(new Song(this.getString(R.string.myplaylist_song_9), this.getString(R.string.myplaylist_singer_2)));
        songs.add(new Song(this.getString(R.string.myplaylist_song_10), this.getString(R.string.myplaylist_singer_2)));

        //LinearLayout rootView = findViewById(R.id.root_view);

        SongAdapter wordItemsAdapter = new SongAdapter(this, songs);
        ListView listView = (ListView)findViewById(R.id.list);
        listView.setAdapter(wordItemsAdapter);
    }
}
