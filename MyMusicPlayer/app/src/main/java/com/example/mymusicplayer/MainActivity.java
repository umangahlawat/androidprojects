
package com.example.mymusicplayer;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the content of the activity to use the activity_main.xml layout file
        setContentView(R.layout.activity_main);

        TrendingClickListener trendingClickListener = new TrendingClickListener();
        //Find the views which shows different categories
        TextView numbers = findViewById(R.id.trending);
        //set click Listener on that view
        numbers.setOnClickListener(trendingClickListener);

        TopTenClickListener topTenClickListener = new TopTenClickListener();
        //Find the views which shows different categories
        TextView colors = findViewById(R.id.top10songs);
        //set click Listener on that view
        colors.setOnClickListener(topTenClickListener);

        MyPlaylistClickListener myPlaylistClickListener = new MyPlaylistClickListener();
        //Find the views which shows different categories
        TextView family = findViewById(R.id.myplaylist);
        //set click Listener on that view
        family.setOnClickListener(myPlaylistClickListener);
    }

}
