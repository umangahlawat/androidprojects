package com.example.mymusicplayer;

import android.content.Intent;
import android.view.View;

public class TopTenClickListener implements View.OnClickListener {
    @Override
    public void onClick(View view) {
        Intent intent = new Intent(view.getContext(), TopTenActivity.class);
        view.getContext().startActivity(intent);
    }
}
