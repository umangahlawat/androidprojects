package com.example.mymusicplayer;

public class Song {
    private String title;
    private String singer;

    public Song(String title, String singer){
        this.title = title;
        this.singer = singer;
    }
    public String getTitle() {
        return title;
    }
    public String getsinger() {
        return singer;
    }
}
