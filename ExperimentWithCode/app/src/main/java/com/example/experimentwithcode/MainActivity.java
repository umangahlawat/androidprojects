package com.example.experimentwithcode;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TextView tv = new TextView(this);
        tv.setText("Pacticing Android!!!!!!");
        tv.setTextSize(50);
        tv.setTextColor(Color.RED);
        setContentView(tv);
    }
}