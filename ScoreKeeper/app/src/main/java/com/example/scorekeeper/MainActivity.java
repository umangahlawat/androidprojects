package com.example.scorekeeper;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private int runsOfTeamA = 0;
    private int wicketFallenOfTeamA = 0;
    private int overPlayedByTeamA = 0;

    private int runsOfTeamB = 0;
    private int wicketFallenOfTeamB = 0;
    private int overPlayedByTeamB = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayForTeamA();
        displayForTeamB();
    }

    //=====================Team A================================
    //onClick of button[+6 Runs] for team A
    public void addSixRunsForTeamA(View view){
        runsOfTeamA += 6;
        displayForTeamA();
    }

    //onClick of button[+4 Runs] for team A
    public void addFourRunsForTeamA(View view){
        runsOfTeamA += 4;
        displayForTeamA();
    }

    //onClick of button[+3 Runs] for team A
    public void addThreeRunsForTeamA(View view){
        runsOfTeamA += 3;
        displayForTeamA();
    }

    //onClick of button[+2 Runs] for team A
    public void addTwoRunsForTeamA(View view){
        runsOfTeamA += 2;
        displayForTeamA();
    }

    //onClick of button[+1 Run] for team A
    public void addOneRunForTeamA(View view){
        runsOfTeamA += 1;
        displayForTeamA();
    }

    //onClick of button[Fall of a wicket] for team A
    public void fallOfWicketForTeamA(View view){
        wicketFallenOfTeamA += 1;
        if(wicketFallenOfTeamA == 10){
            disableButtonsForTeamA();
        }
        displayForTeamA();
    }

    //onClick of button[Over played increased] for team A
    public void overPlayedForTeamA(View view){
        overPlayedByTeamA += 1;
        if(overPlayedByTeamA == 20){
            disableButtonsForTeamA();
        }
        displayForTeamA();
    }

    //updating text view for team A
    public void displayForTeamA(){
        TextView scoreView = findViewById(R.id.team_a_score);
        scoreView.setText(String.valueOf(runsOfTeamA)+"/"+String.valueOf(wicketFallenOfTeamA)+"\nOvers:"+String.valueOf(overPlayedByTeamA));
    }

    //when overs reached to 20 or all 10 wickts fall, disable buttons
    public void disableButtonsForTeamA(){
        findViewById(R.id.six_for_team_a).setEnabled(false);
        findViewById(R.id.four_for_team_a).setEnabled(false);
        findViewById(R.id.three_for_team_a).setEnabled(false);
        findViewById(R.id.two_for_team_a).setEnabled(false);
        findViewById(R.id.one_for_team_a).setEnabled(false);
        findViewById(R.id.wicket_fall_for_team_a).setEnabled(false);
        findViewById(R.id.over_played_for_team_a).setEnabled(false);
    }

    //enable all buttons for team A on click of reset button
    public void enableButtonsForTeamA(){
        findViewById(R.id.six_for_team_a).setEnabled(true);
        findViewById(R.id.four_for_team_a).setEnabled(true);
        findViewById(R.id.three_for_team_a).setEnabled(true);
        findViewById(R.id.two_for_team_a).setEnabled(true);
        findViewById(R.id.one_for_team_a).setEnabled(true);
        findViewById(R.id.wicket_fall_for_team_a).setEnabled(true);
        findViewById(R.id.over_played_for_team_a).setEnabled(true);
    }
    //=====================Team B================================
    //onClick of button[+6 Runs] for team B
    public void addSixRunsForTeamB(View view){
        runsOfTeamB += 6;
        displayForTeamB();
    }

    //onClick of button[+4 Runs] for team B
    public void addFourRunsForTeamB(View view){
        runsOfTeamB += 4;
        displayForTeamB();
    }

    //onClick of button[+3 Runs] for team B
    public void addThreeRunsForTeamB(View view){
        runsOfTeamB += 3;
        displayForTeamB();
    }

    //onClick of button[+2 Runs] for team B
    public void addTwoRunsForTeamB(View view){
        runsOfTeamB += 2;
        displayForTeamB();
    }

    //onClick of button[+1 Run] for team B
    public void addOneRunForTeamB(View view){
        runsOfTeamB += 1;
        displayForTeamB();
    }

    //onClick of button[Fall of a wicket] for team B
    public void fallOfWicketForTeamB(View view){
        wicketFallenOfTeamB += 1;
        if(wicketFallenOfTeamB == 10){
            disableButtonsForTeamB();
        }
        displayForTeamB();
    }

    //onClick of button[Over played increased] for team B
    public void overPlayedForTeamB(View view){
        overPlayedByTeamB += 1;
        if(overPlayedByTeamB == 20){
            disableButtonsForTeamB();
        }
        displayForTeamB();
    }

    //updating text view for team B
    public void displayForTeamB(){
        TextView scoreView = findViewById(R.id.team_b_score);
        scoreView.setText(String.valueOf(runsOfTeamB)+"/"+String.valueOf(wicketFallenOfTeamB)+"\nOvers:"+String.valueOf(overPlayedByTeamB));
    }

    //when overs reached to 20 or all 10 wickts fall, disable buttons
    public void disableButtonsForTeamB(){
        findViewById(R.id.six_for_team_b).setEnabled(false);
        findViewById(R.id.four_for_team_b).setEnabled(false);
        findViewById(R.id.three_for_team_b).setEnabled(false);
        findViewById(R.id.two_for_team_b).setEnabled(false);
        findViewById(R.id.one_for_team_b).setEnabled(false);
        findViewById(R.id.wicket_fall_for_team_b).setEnabled(false);
        findViewById(R.id.over_played_for_team_b).setEnabled(false);
    }

    //enable all buttons for team B on click of reset button
    public void enableButtonsForTeamB(){
        findViewById(R.id.six_for_team_b).setEnabled(true);
        findViewById(R.id.four_for_team_b).setEnabled(true);
        findViewById(R.id.three_for_team_b).setEnabled(true);
        findViewById(R.id.two_for_team_b).setEnabled(true);
        findViewById(R.id.one_for_team_b).setEnabled(true);
        findViewById(R.id.wicket_fall_for_team_b).setEnabled(true);
        findViewById(R.id.over_played_for_team_b).setEnabled(true);
    }

    // reset the score of both team's to 0
    public void resetScore(View view){
        runsOfTeamA = 0;
        wicketFallenOfTeamA = 0;
        overPlayedByTeamA = 0;

        runsOfTeamB = 0;
        wicketFallenOfTeamB = 0;
        overPlayedByTeamB = 0;

        displayForTeamA();
        displayForTeamB();

        enableButtonsForTeamA();
        enableButtonsForTeamB();
    }
}