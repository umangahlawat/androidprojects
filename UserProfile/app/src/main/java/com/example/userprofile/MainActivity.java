package com.example.userprofile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView name;
    private TextView dob;
    private TextView country;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.name);
        dob = findViewById(R.id.birthday);
        country = findViewById(R.id.country);
        image = findViewById(R.id.profile_picture);

        name.setText("Aarav Ahlawat");
        dob.setText("10th Oct2020");
        country.setText("India");
        image.setImageDrawable(getDrawable(R.drawable.ic_launcher_foreground));
    }
}