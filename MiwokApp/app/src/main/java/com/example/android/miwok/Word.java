package com.example.android.miwok;

import android.widget.ImageView;

public class Word {
    private String miwokTranslation;
    private String defaultTranslation;
    private int imageResourceId = -1;
    private int soundResourceId = -1;
    public static final int NO_IMAGE_STATE = -1;

    public Word(String miwokTranslation, String defaultTranslation){
        this.miwokTranslation = miwokTranslation;
        this.defaultTranslation = defaultTranslation;
    }

    public Word(String miwokTranslation, String defaultTranslation, int imageResourceId){
        this.miwokTranslation = miwokTranslation;
        this.defaultTranslation = defaultTranslation;
        this.imageResourceId = imageResourceId;
    }

    public Word(String miwokTranslation, String defaultTranslation, int imageResourceId, int soundResourceId){
        this.miwokTranslation = miwokTranslation;
        this.defaultTranslation = defaultTranslation;
        this.imageResourceId = imageResourceId;
        this.soundResourceId = soundResourceId;
    }

    public String getMiwokTranslation() {
        return miwokTranslation;
    }
    public String getDefaultTranslation() {
        return defaultTranslation;
    }
    public int getImageResourceId() {
        return imageResourceId;
    }
    public int getSoundResourceId() {
        return soundResourceId;
    }
    public  boolean getImageState() {
        return imageResourceId != NO_IMAGE_STATE ? true : false;
    }
}
