package com.example.android.miwok;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

public class NumbersClickListener implements View.OnClickListener {
    @Override
    public void onClick(View view) {
        Intent intent = new Intent(view.getContext(), NumberActivity.class);
        view.getContext().startActivity(intent);
    }
}
