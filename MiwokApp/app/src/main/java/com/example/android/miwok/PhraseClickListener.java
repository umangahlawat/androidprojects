package com.example.android.miwok;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

public class PhraseClickListener implements View.OnClickListener {
    @Override
    public void onClick(View view) {
        Intent intent = new Intent(view.getContext(), PhraseActivity.class);
        view.getContext().startActivity(intent);
    }
}
