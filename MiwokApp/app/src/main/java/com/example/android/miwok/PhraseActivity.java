package com.example.android.miwok;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class PhraseActivity extends AppCompatActivity {
    private MediaPlayer myPlayer;
    private AudioManager am;
    private AudioManager.OnAudioFocusChangeListener afChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                public void onAudioFocusChange(int focusChange) {
                    if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                            focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                        myPlayer.pause();
                        myPlayer.seekTo(0);
                    } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                        myPlayer.stop();
                        Utils.releaseMediaPlayer(myPlayer, am, afChangeListener);

                        am.abandonAudioFocus(afChangeListener);
                    }  else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                        myPlayer.start();
                    }
                }
            };

    private MediaPlayer.OnCompletionListener mOnCompletionListener= new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            Utils.releaseMediaPlayer(myPlayer, am, afChangeListener);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_list);
        final ArrayList<Word> words = new ArrayList<>();
        words.add(new Word("minto wuksus", "Where are you going?", -1, R.raw.phrase_where_are_you_going));
        words.add(new Word("tinnә oyaase'nә", "What is your name?", -1, R.raw.phrase_what_is_your_name));
        words.add(new Word("oyaaset...", "My name is...", -1, R.raw.phrase_my_name_is));
        words.add(new Word("michәksәs?", "How are you feeling?", -1, R.raw.phrase_how_are_you_feeling));
        words.add(new Word("kuchi achit", "I’m feeling good.", -1, R.raw.phrase_im_feeling_good));
        words.add(new Word("әәnәs'aa?", "Are you coming?", - 1, R.raw.phrase_are_you_coming));
        words.add(new Word("hәә’ әәnәm", "Yes, I am coming.", -1, R.raw.phrase_yes_im_coming));
        words.add(new Word("әәnәm", "I’m coming.", -1, R.raw.phrase_im_coming));
        words.add(new Word("yoowutis", "Let’s go.", -1, R.raw.phrase_lets_go));
        words.add(new Word("әnni'nem", "come here", -1, R.raw.phrase_come_here));

        //LinearLayout rootView = findViewById(R.id.root_view);

        WordAdapter wordItemsAdapter = new WordAdapter(this, words, R.color.category_phrases);
        ListView listView = (ListView)findViewById(R.id.list);
        listView.setAdapter(wordItemsAdapter);

        am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Word word = words.get(i);
                Utils.releaseMediaPlayer(myPlayer, am, afChangeListener);
                // Request audio focus for playback
                int result = am.requestAudioFocus(afChangeListener,
                        // Use the music stream.
                        AudioManager.STREAM_MUSIC,
                        // Request temp focus.
                        AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);

                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    // Start playback
                    myPlayer = MediaPlayer.create(PhraseActivity.this, word.getSoundResourceId());
                    myPlayer.start();
                }
                myPlayer.setOnCompletionListener(mOnCompletionListener);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        Utils.releaseMediaPlayer(myPlayer, am, afChangeListener);
    }
}
