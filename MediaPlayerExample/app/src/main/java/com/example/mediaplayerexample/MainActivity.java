package com.example.mediaplayerexample;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    Button play;
    Button pause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        play = findViewById(R.id.play);
        pause = findViewById(R.id.pause);

        final MediaPlayer myPlayer = MediaPlayer.create(this, R.raw.hindiproject);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myPlayer.start();
                myPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        Toast.makeText(MainActivity.this, "I am Done !!", Toast.LENGTH_LONG).show();
                        Log.d("MediaPlayerExample", "setOnCompletionListener");
                    }
                });
            }
        });

        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myPlayer.pause();
            }
        });
    }
}