package com.example.quizapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static int REQ_CODE = 1;
    private String userNameToShow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startQuiz(View v) {
        EditText userName = findViewById(R.id.user_name);
        Editable name = userName.getText();
        if (name != null && name.toString().trim().length() > 0) {
            userNameToShow = name.toString();
            Intent intent = new Intent(this, QuestionActivity.class);
            startActivityForResult(intent, REQ_CODE);
        }else{
            Toast.makeText(this, "Kindly enter user name!!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE) {
            Toast.makeText(this, "Your score == " + data.getIntExtra("score", 0) + "     Wrong Ans : " + data.getIntExtra("wrong", 0), Toast.LENGTH_LONG).show();
            TextView thankYouNote = findViewById(R.id.thankyou);
            thankYouNote.setText("Thank you " + userNameToShow +" for completing quiz!!\n" + "Your score : " + data.getIntExtra("score", 0));
        }
    }
}