package com.example.quizapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Button;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

public class QuestionActivity extends AppCompatActivity {
    private Map<Integer, ArrayList<String>> map = new HashMap<>();
    //declaring variables
    private TextView ques1;
    private RadioButton q1rb1;
    private RadioButton q1rb2;
    private RadioButton q1rb3;

    private TextView ques2;
    private RadioButton q2rb1;
    private RadioButton q2rb2;
    private RadioButton q2rb3;

    private TextView ques3;
    private RadioButton q3rb1;
    private RadioButton q3rb2;
    private RadioButton q3rb3;

    private TextView ques4;
    private CheckBox q4cb1;
    private CheckBox q4cb2;
    private CheckBox q4cb3;

    private TextView ques5;
    private EditText q5et1;

    private int score = 0;
    private int wrong = 0;
    private Button button;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //question map to be shown in quiz
        map.put(1, new ArrayList<String>() {
            {
                add("First prime minister of india :");
                add("AB Vajpayee");
                add("Narendra Modi");
                add("Nehru");
            }
        });

        map.put(2, new ArrayList<String>() {
            {
                add("National animal of India :");
                add("Tiger");
                add("Elephant");
                add("Cow");
            }
        });

        map.put(3, new ArrayList<String>() {
            {
                add("Capital of India :");
                add("Mumbai");
                add("Delhi");
                add("Chennai");
            }
        });

        map.put(4, new ArrayList<String>() {
            {
                add("Former captains of Indian cricket team");
                add("Virat");
                add("Dhoni");
                add("Saurav");
            }
        });

        map.put(5, new ArrayList<String>() {
            {
                add("Financial capital of India ?");
                add("mumbai");
            }
        });

        setContentView(R.layout.question);

        // getting view from id
        ques1 = findViewById(R.id.ques1_tv);
        q1rb1 = findViewById(R.id.ans1_rb1);
        q1rb2 = findViewById(R.id.ans1_rb2);
        q1rb3 = findViewById(R.id.ans1_rb3);

        ques2 = findViewById(R.id.ques2_tv);
        q2rb1 = findViewById(R.id.ans2_rb1);
        q2rb2 = findViewById(R.id.ans2_rb2);
        q2rb3 = findViewById(R.id.ans2_rb3);

        ques3 = findViewById(R.id.ques3_tv);
        q3rb1 = findViewById(R.id.ans3_rb1);
        q3rb2 = findViewById(R.id.ans3_rb2);
        q3rb3 = findViewById(R.id.ans3_rb3);

        ques4 = findViewById(R.id.ques4_tv);
        q4cb1 = findViewById(R.id.cb1);
        q4cb2 = findViewById(R.id.cb2);
        q4cb3 = findViewById(R.id.cb3);

        ques5 = findViewById(R.id.ques5_tv);
        q5et1 = findViewById(R.id.ans5);

        button = findViewById(R.id.button);

        // setting questions and options of quiz for all four questions
        ques1.setText(map.get(1).get(0));
        q1rb1.setText(map.get(1).get(1));
        q1rb2.setText(map.get(1).get(2));
        q1rb3.setText(map.get(1).get(3));

        ques2.setText(map.get(2).get(0));
        q2rb1.setText(map.get(2).get(1));
        q2rb2.setText(map.get(2).get(2));
        q2rb3.setText(map.get(2).get(3));

        ques3.setText(map.get(3).get(0));
        q3rb1.setText(map.get(3).get(1));
        q3rb2.setText(map.get(3).get(2));
        q3rb3.setText(map.get(3).get(3));

        ques4.setText(map.get(4).get(0));
        q4cb1.setText(map.get(4).get(1));
        q4cb2.setText(map.get(4).get(2));
        q4cb3.setText(map.get(4).get(3));

        ques4.setText(map.get(5).get(0));
    }

    //Handling onclick on finish button and sending score to previous activity
    public void finish(View v) {
        score = 0;
        wrong = 0;
        if (q1rb3.isChecked()) {
            score++;
        } else if (q1rb2.isChecked() || q1rb1.isChecked()) {
            wrong++;
        }

        if (q2rb1.isChecked()) {
            score++;
        } else if(q2rb2.isChecked() || q2rb3.isChecked()){
            wrong++;
        }

        if (q3rb2.isChecked()) {
            score++;
        }else if(q3rb1.isChecked() || q3rb3.isChecked()){
            wrong++;
        }

        if (!q4cb1.isChecked() && q4cb2.isChecked() && q4cb3.isChecked()) {
            score++;
        }else if (q4cb1.isChecked() || q4cb2.isChecked() || q4cb3.isChecked()) {
            wrong++;
        }

        if (q5et1.getText() != null && q5et1.getText().toString().toLowerCase().equals(map.get(5).get(1))) {
            score++;
        }else if(q5et1.getText() != null){
            wrong++;
        }

        Intent resultIntent = new Intent();
        resultIntent.putExtra("score", score);
        resultIntent.putExtra("wrong", wrong);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }
}
