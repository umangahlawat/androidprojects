package com.example.android.miwok;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class CategoryAdapter extends FragmentPagerAdapter {
    public CategoryAdapter(FragmentManager fm){
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }


    private String tabTitles[] = new String[] { "Numbers", "Colors", "Family", "Phrases" };

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            return new NumberFragment();
        }else if(position == 1){
            return new ColorFragment();
        }else if(position == 2){
            return new FamilyFragment();
        }else{
            return new PhraseFragment();
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
