package com.example.android.miwok;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import androidx.annotation.Nullable;

public class ColorFragment extends Fragment {
    private AudioManager am;
    private MediaPlayer myPlayer;
    private AudioManager.OnAudioFocusChangeListener afChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                public void onAudioFocusChange(int focusChange) {
                    if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                            focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                        myPlayer.pause();
                        myPlayer.seekTo(0);
                    } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                        myPlayer.stop();
                        Utils.releaseMediaPlayer(myPlayer, am, afChangeListener);
                    }  else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                        myPlayer.start();
                    }
                }
            };

    public  ColorFragment(){

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = inflater.inflate(R.layout.word_list, container, false);

        ArrayList<Word> words = new ArrayList<>();
        words.add(new Word("weṭeṭṭi", "red", R.drawable.color_red, R.raw.color_red));
        words.add(new Word("chokokki", "green", R.drawable.color_green, R.raw.color_green));
        words.add(new Word("ṭakaakki", "brown", R.drawable.color_brown, R.raw.color_brown));
        words.add(new Word("ṭopoppi", "gray", R.drawable.color_gray, R.raw.color_gray));
        words.add(new Word("kululli", "black", R.drawable.color_black, R.raw.color_black));
        words.add(new Word("kelelli", "white", R.drawable.color_white, R.raw.color_white));
        words.add(new Word("ṭopiisә", "dusky yellow", R.drawable.color_dusty_yellow, R.raw.color_dusty_yellow));
        words.add(new Word("chiwiiṭә", "mustard yello", R.drawable.color_mustard_yellow, R.raw.color_mustard_yellow));

        //LinearLayout rootView = findViewById(R.id.root_view);

        WordAdapter wordItemsAdapter = new WordAdapter(getActivity(), words, R.color.category_colors);
        ListView listView = (ListView)rootView.findViewById(R.id.list);
        listView.setAdapter(wordItemsAdapter);

        am = (AudioManager)(getActivity().getSystemService(Context.AUDIO_SERVICE));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Utils.releaseMediaPlayer(myPlayer, am, afChangeListener);
                // Request audio focus for playback
                int result = am.requestAudioFocus(afChangeListener,
                        // Use the music stream.
                        AudioManager.STREAM_MUSIC,
                        // Request temp focus.
                        AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);

                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    // Start playback
                    myPlayer = MediaPlayer.create(getActivity(), ((Word)(adapterView.getItemAtPosition(i))).getSoundResourceId());
                    myPlayer.start();
                }
                myPlayer.setOnCompletionListener(mOnCompletionListener);
            }
        });

        return rootView;
    }

    private MediaPlayer.OnCompletionListener mOnCompletionListener= new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            Utils.releaseMediaPlayer(myPlayer, am, afChangeListener);
        }
    };


    @Override
    public void onStop() {
        super.onStop();
        Utils.releaseMediaPlayer(myPlayer, am, afChangeListener);
    }
}

