package com.example.android.miwok;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class WordAdapter extends ArrayAdapter<Word> {
    int bgColorResourceId;
    public  WordAdapter(Activity context, ArrayList<Word> wordArrayList, int bgColorResourceId){
        super(context, 0, wordArrayList);
        this.bgColorResourceId = bgColorResourceId;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }
        TextView miwokText = listItemView.findViewById(R.id.miwokText);
        TextView defaultText = listItemView.findViewById(R.id.engText);
        ImageView imageView = listItemView.findViewById(R.id.image);
        LinearLayout textContainer = listItemView.findViewById(R.id.textContainer);

        Word currWord = getItem(position);

        miwokText.setText(currWord.getMiwokTranslation());
        defaultText.setText(currWord.getDefaultTranslation());

        if(currWord.getImageState()) {
            imageView.setImageResource(currWord.getImageResourceId());
            imageView.setVisibility(View.VISIBLE);
        }else{
            imageView.setVisibility(View.GONE);
        }

        textContainer.setBackgroundColor(getContext().getColor(bgColorResourceId));
        return listItemView;
    }
}
