package com.example.thelearningapplesson1project;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

/*adopted from http://ruibm.com/2009/06/16/rounded-corner-bitmaps-on-android/*/
//https://inducesmile.com/android/how-to-make-circular-imageview-and-rounded-corner-imageview-in-android/

/*
* A simple way to achieve this is by using a CardView widget, and placing an ImageView inside it.
* The CardView allows you to specify a corner radius, in order to achieve a circular container for the ImageView.  implementation "androidx.cardview:cardview:1.0.0"
*/

public class ImageConverter {

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }
}
