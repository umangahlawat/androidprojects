package com.example.thelearningapplesson1project;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toolbar;

public class MainActivity extends AppCompatActivity {
    public Toolbar mToolBar;
    public Bitmap bitmap;
    public Bitmap circularBitmap;
    public ImageView circularImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.profilepic);
        circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 500);

        circularImageView = (ImageView)findViewById(R.id.profileimage);
        circularImageView.setImageBitmap(circularBitmap);


        bitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.pencileditprofile);
        circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 500);

        circularImageView = (ImageView)findViewById(R.id.userdetaileditbutton);
        circularImageView.setImageBitmap(circularBitmap);

        mToolBar = findViewById(R.id.toolbar);
        setActionBar(mToolBar);
        getActionBar().show();
    }
}